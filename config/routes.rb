Rails.application.routes.draw do
  root to: 'visitors#index'
  
  get "/contact", to: "pages#contact"
  get "/jobs",    to: "pages#jobs"
  get "/menu",    to: "pages#menu"
end
